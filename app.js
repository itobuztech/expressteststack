const express = require('express')
const app = express()
const mongoose = require('mongoose')
var bodyParser = require('body-parser')
const UserRoute = require('./routes/UserRoute')
const PostRoute = require('./routes/PostRoute')
const FormRoute = require('./routes/FormRoute')
const passport = require('passport')
require('dotenv').config();
const PORT = 3001
const Postmodel = require("./models/post");
const CustomError=require('./errors/custom.error')
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())
app.use(bodyParser.json())
require('./models/connection')
  mongoose.set('useFindAndModify', false)
  mongoose.set('useCreateIndex', true)
  app.use('/api/PostOperation', PostRoute)
  app.use('/api/Useroperation', UserRoute)
  app.use('/api/User',FormRoute)
  app.use(CustomError.CustomError)
  app.use(CustomError.DefaultError)
  const server=app.listen(PORT, () => {
    console.log(`node server connected on port ${PORT}`)
  })
  



