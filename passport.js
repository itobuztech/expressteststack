const passport = require('passport')
const passportStrategy = require('passport-local').Strategy
const passportJWT = require('passport-jwt')
const JWTStrategy = passportJWT.Strategy
const ExtractJWT = passportJWT.ExtractJwt
const usermodel = require('./models/user')
const bcrypt = require('bcrypt')
const Joi = require('@hapi/joi')
const{ValidationError,HttpError}=require('./errors')


  passport.use(
  new passportStrategy(
    {
      usernameField: 'email',
      passwordField: 'password'
    },
    async function (email,password,cb) {
      try {
        const userlogSchema = Joi.object({
          email: Joi.string().required(),
          password: Joi.string().required()
        })
        const { error, testvalue } = userlogSchema.validate({ email: email, password: password })
        if (error) {
          return cb(null, false, { message: 'Please field all details', response: error })
        } else {
          const userdata = await usermodel.findOne({ email })
          if (userdata) {
            const match = await bcrypt.compare(password, userdata.password)
            if (match) {
              const jwtPayload = {
                id: userdata.id,
                name: userdata.name,
                email: userdata.email
              }
              return cb(null, jwtPayload, { message: 'successfully logged in', response: userdata })
            } else {
              return cb(null, false, { message: 'Invalid username or password', response: match })
            }
          } else {
            return cb(null, false, { message: 'user not found', response: userdata })
          }
        }
      } catch (err) {
        return cb(err)
      }
    }
  )
)

passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: 'SECRET'
    },

    async function (jwtPayload, cb) {
      // console.log(jwtPayload)
      try {
        try {
          const userjwtchk = await usermodel.findOne({ email: jwtPayload.email })
          // console.log(userjwtchk)
          if (userjwtchk) {
            const authuserdata = {
              id: userjwtchk.id,
              email: userjwtchk.email
            }
            // console.log(authuserdata)
            return cb(null, authuserdata, { message: 'valid token', response: authuserdata })
          } else {
            return cb(null, false, { message: 'user not found', response: userjwtchk })
          }
        } catch (err) {
          return cb(err)
        }
      } catch (err) {
        return cb(err)
      }
    }
  )
)
