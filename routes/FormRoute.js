const express = require('express')
const FormRoute = express.Router()
const formcontroller = require('../controllers/FormController')
// newsletter subscription
FormRoute.post('/newsletter-subscription', formcontroller.NewsLetterSubscription)

module.exports = FormRoute
