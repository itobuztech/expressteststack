const express = require("express");
const userasyncrouter = express.Router();
const userasyncawaitController = require("../controllers/UserController");
const passport = require("passport");
const authentication = require("../controllers/AuthController");
require("../passport");
// User registration
userasyncrouter.post("/add-user", userasyncawaitController.userregistration);
// User login
userasyncrouter.post("/login", passport.authenticate("local", { session: false }), authentication);
// Get All User Details
userasyncrouter.get("/list-user", passport.authenticate("jwt", { session: false }), userasyncawaitController.userAllDetails);
// Get Single User Details
userasyncrouter.get("/user-details/:userid", passport.authenticate("jwt", { session: false }), userasyncawaitController.singleUser);
// Update User Data
userasyncrouter.put("/update-user/:userid", passport.authenticate("jwt", { session: false }), userasyncawaitController.userUpdate);
// Delete User Details
userasyncrouter.delete("/delete-user/:userid", passport.authenticate("jwt", { session: false }), userasyncawaitController.deleteUser);

module.exports = userasyncrouter;
