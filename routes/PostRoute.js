const express = require('express')
const Postroute = express.Router()
const PostController = require('../controllers/PostController')
const passport = require('passport')
require("../passport");

// create a new post by user
Postroute.post('/addpost', passport.authenticate('jwt',{session: false}),PostController.createpost)

//Postroute.post('/addpost', passport.authenticate('jwt', { session: false }), PostController.createpost)
// getting all posts for user
Postroute.get('/allposts',passport.authenticate('jwt',{session: false}), PostController.AllPost)
// modified post
Postroute.put('/updatepost',passport.authenticate('jwt',{session: false}),PostController.ModifiedPost)
// delete post
Postroute.delete('/deletepost/:postId',passport.authenticate('jwt',{session: false}),PostController.DeletePost)

module.exports = Postroute
