const frisby = require("frisby");
const baseUrl = 'localhost:3001/api'

describe('User module test', () => {
it('login', function () {
    return frisby.post(baseUrl + '/Useroperation/login', {
      email: 'sanjukta@cpsp.in',
      password: '123456'
    })
      .expect('status', 200)
      .expect('json', 'statusCode', 200)
      .then(function (res) {
        let token = res.json.token;
        return frisby.globalSetup({
          request: {
            headers: {
              'Authorization': 'Bearer ' + token,
              'Content-Type': 'application/json',
            }
          }
        });

      });
  });

it('Add User', function() {
    return frisby
        .post(baseUrl + '/Useroperation/add-user',{
         "name":"sanjukta Das",
        "address":"kolkata",
        "country_code":91,
        "phone_number":"7412589630",
        "email":"sanjukta@cpsp.in",
        "password":"123456"
        })
        .expect('status', 201)
        .expect('json', 'statusCode', 201)
        .expect('json', 'message', 'User added successfully')
});

it('Retrive all users', function() {
    return frisby
    .get(baseUrl + '/Useroperation/list-user')
    .expect('status', 200)
    .expect('json', 'statusCode', 200)
    .expect('json', 'message', 'User has been listed successfully')
});

it('Retrive single user', function() {
    return frisby
    .get(baseUrl + '/Useroperation/user-details/1')
    .expect('status', 200)
    .expect('json', 'statusCode', 200)
    .expect('json', 'message', 'User has been listed successfully')
});

it ('Update user', function () {
    return frisby
      .put(baseUrl + '/Useroperation/update-user/1', {
        "name":"sanjukta Das",
        "address":"kolkata",
        "country_code":91,
        "phone_number":"8520123654"
      })
      .expect('status', 200)
      .expect('json', 'statusCode', 200)
      .expect('json', 'message', 'User has been updated successfully')
  });

  it ('Delete user', function () {
    return frisby
      .del(baseUrl + '/Useroperation/delete-user/1')
      .expect('status', 204)
      .expect('json', 'statusCode', 200)
      .expect('json', 'message', 'User deleted successfully')
  });

})
