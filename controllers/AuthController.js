const jwt = require('jsonwebtoken')
const passport = require('passport')
require('../passport')

const authentication = async (req, res) => {
  if (!req.user) {
    return res.status(400).json({
      message: 'Something is not right',
      user: req.user
    })
  } else {
    const token = jwt.sign(req.user, 'SECRET')
    return res.json({ user: req.user, token })
  }
}
module.exports = authentication
