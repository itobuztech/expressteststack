const UserService=require('../services/UserService')
const UserSchema=require('./schemas/UserSchema')
const bcrypt = require('bcrypt')
const{ValidationError,HttpError}=require('../errors')
const Joi = require('@hapi/joi')
// User registration
const userregistration = async (req, res,next) => {
  try {
    const userdata = {
      name: req.body.name,
      address: req.body.address,
      country_code:req.body.country_code,
      phone_number: req.body.phone_number,
      email: req.body.email,
      password: req.body.password
    }
    const {error,value} = UserSchema.validate(userdata)
    if (error) {
      const ErrorData=[]
      const ErrorDetails=error.details
      ErrorDetails.forEach((errresult)=>{
        ErrorData.push(errresult.message)
      })
      next(new ValidationError(400,'validation failed',ErrorData))
     // next(new ValidationError(400,'validation failed',error.details))
    } else {
      //process.exit(1)
      const hashing=await bcrypt.hash(value.password,10)
      if(hashing){
        const resultData=await UserService.Signup(value,hashing);
        if(resultData){
          res.status(201).json({
            status:true,
            message:'User added successfully',
            data:resultData,
            statusCode:201
          })
        }else{
          next(new HttpError(406,'User has not been added',resultData))
        }
      }else{
        next(new HttpError(406,'Password encryption failed',hashing))
      }
    } 
  }catch (err) {
   next(new HttpError(500,'Internal server occured',err))
 }
}
const userAllDetails = async (req, res,next) => {
  try {
    const userdata = await UserService.UserAllDetail();
    if (userdata) {
      res.status(200).json({
        status:true,
        message:'User has been listed successfully',
        data:userdata,
        statusCode:200
      })
    } else {
      next(new HttpError(404,'User has not been listed successfully',{}))
    }
  } catch (err) {
   next(new HttpError(500,'Internal server occured',err))
 }
}
const singleUser = async (req, res,next) => {
  try {
    const id = req.params.userid
    const getsingleuserdata = await UserService.SingleUserDetail(id)
    if (getsingleuserdata) {
      res.status(200).json({
        status:true,
        message:'User has been listed successfully',
        data:getsingleuserdata,
        statusCode:200
      })
    } else {
      next(new HttpError(404,'User has not been listed successfully',{}))
    }
  } catch (err) {
    next(new HttpError(500,'Internal server occured',err))
  }
}
const userUpdate = async (req, res,next) => {
  try {
    var id = req.params.userid
    const getUser=await UserService.SingleUserDetail(id)
    if(getUser){
      var userupdatedData = {
        name: req.body.name,
        address: req.body.address,
        country_code:req.body.country_code,
        phone_number: req.body.phone_number
      }
      const userupdateSchema = Joi.object({
        name: Joi.string().required(),
        address: Joi.string().required(),
        country_code:Joi.required(),
        phone_number: Joi.required()
      })
      const {error,value} = userupdateSchema.validate(userupdatedData)
      if (error) {
        const ErrorData=[]
        const ErrorDetails=error.details
        ErrorDetails.forEach((errresult)=>{
          ErrorData.push(errresult.message)
        })
        next(new ValidationError(400,'validation failed',ErrorData))
      } else {
        const updateddata=await UserService.UpdateUser(id,userupdatedData)
        if(updateddata){
          const updateduserDetails=await UserService.SingleUserDetail(id)
          res.status(200).json({
            status:true,
            message: 'User has been updated successfully', 
            data: updateduserDetails, 
            status_code: 200 
          })
        }else{
         next(new HttpError(304,'User has not been updated successfully',updateddata)) 
       }
     }
   }else{
     next(new HttpError(404,'User not exist',{}))
   } 
 }
 catch (err) {
   next(new HttpError(500,'Internal server occured',err))
 }
}
const deleteUser = async (req, res,next) => {
  try {
    const id = req.params.userid
    const getUser=await UserService.SingleUserDetail(id)
    if(getUser){
      const deletedresult = await UserService.DeleteUser(id)
      if (deletedresult) {
        res.status(204).json({
          status:true,
          message: 'User deleted successfully', 
          data: deletedresult, 
          status_code: 204 
        })
      } else {
        next(new HttpError(406,'User has not been deleted successfully',deletedresult))
      }
    }else{
     next(new HttpError(404,'User not exist',{}))
   }
 } catch (err) {
  next(new HttpError(500,'Internal server occured',err))
}
}
module.exports = {
  userregistration,
  userAllDetails,
  singleUser,
  deleteUser,
  userUpdate
}
