const Postmodel = require('../models/post')
const Usermodel = require('../models/user')
const Joi = require('@hapi/joi')
const{ValidationError,HttpError}=require('../errors')
const PostSchema=require('./schemas/PostSchema')
const PostService=require('../services/PostService')
// create post
const createpost=async(req,res,next)=>{
  try {
      const postdata = {
       title: req.body.title,
       subtitle: req.body.subtitle,
       message: req.body.message
     }
     const {error,value} = PostSchema.validate(postdata)
     if (error) {
       const ErrorData=[]
       const ErrorDetails=error.details
       ErrorDetails.forEach((errresult)=>{
        ErrorData.push(errresult.message)
      })
       next(new ValidationError(400,'validation failed',ErrorData))
     } else {
      const addnewpostData=await PostService.addingPost(value,req.user.id)
      if(addnewpostData.status){
        res.json({
        message: addnewpostData.message,
        statusCode: addnewpostData.statusCode
        })
      }
     }
  }catch(err){
    next(new HttpError(500,'Internal server error',err))
  }
}

/*function getQuote(presult) {
  return new Promise(function(resolve, reject) {
    client.messages.create({
        to: '+'+presult.country_code+presult.phone_number,
        from: "+13343731984",
         body: 'New post added in itnews.Please login to see that post'
       },(twilio_error,twilio_result)=>{
         if(twilio_error){
           return reject(twilio_error)
         }else{
           return resolve(twilio_result)
         }
       })
  })
}*/
// get all post list
const AllPost = async (req,res,next) => {
  try {
    var userId = req.user.id
    const appPostData=await PostService.GetAllPost(userId)
    if(appPostData.status){
      res.json({
        message: appPostData.message,
        data:appPostData.resultData,
        statusCode: appPostData.statusCode
        })
    }else{
      next(new HttpError(404,'Your post has not been found',{}))
    }
  } catch (err) {
    next(new HttpError(500,'Internal server error',err))
  }
}

// modified post
const ModifiedPost = async (req, res,next) => {
  try {
 		const userId = req.user.id
    var postId = req.body.postid
      const PostModifiedData = {
        title: req.body.title,
        subtitle: req.body.subtitle,
        message: req.body.message
        }
        const updatedPost = await PostService.updateService(postId,userId,PostModifiedData)
        if(updatedPost.status){
          res.json({
          message: updatedPost.message,
          statusCode: updatedPost.statusCode
        })
       } else {
        res.json({
          message: updatedPost.message,
          statusCode: updatedPost.statusCode
        })
      }
} catch (err) {
  next(new HttpError(500,'Internal server occured',err))
}
}
/**
  * Delete particular post by user
  */
  const DeletePost = async (req, res,next) => {
   try {
    var postId = req.params.postId
    const deletedPost=await PostService.deletePostService(postId)
    if(deletedPost.status){
      res.json({
        message: deletedPost.message,
        data:{
          postexist:deletedPost.status
        },
        statusCode: deletedPost.statusCode
      })
    }else{
      res.json({
        message: deletedPost.message,
        data:{
          postexist:deletedPost.status
        },
        statusCode: deletedPost.statusCode
      })
    }
    } catch (dberror) {
      next(new HttpError(500,'Internal server occured',dberror))
    }
}
module.exports = {
  createpost,
  AllPost,
  ModifiedPost,
  DeletePost
}
