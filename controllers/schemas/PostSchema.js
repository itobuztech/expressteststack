const Joi = require('@hapi/joi')

module.exports = Joi.object({
  title: Joi.string().required().messages({
    "string.base": "Title must be string",
    "any.required":"Title is required",
    "string.empty":"Title is empty"
  }),
  subtitle: Joi.string().required().messages({
    "any.required":"Subtitle is required",
    "string.empty":"Subtitle is empty",
    "string.base": "Subtitle must be string"
  }),
   message: Joi.string().required().messages({
    "any.required":"Message is required",
    "string.empty":"Message is empty"
  })
})


