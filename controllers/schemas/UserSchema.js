const Joi = require('@hapi/joi')

module.exports = Joi.object({
  name: Joi.string().regex(/^[A-Za-z]+/).required().messages({
    "string.base": "Name must be string",
    "string.pattern.base":"Name must not have numbers",
    "any.required":"Name is required",
    "string.empty":"Name is empty"
  }),
  address: Joi.string().required().messages({
    "any.required":"address is required",
    "string.empty":"address is empty",
    "string.base": "address must be string",
  }),
  country_code: Joi.number().min(1).max(9999).empty().required().messages({
    "number.min": "Invalid country code",
    "any.required":"country code is required",
    "number.base":"Invalid country code",
    "number.max":"Invalid country code"
  }),
   phone_number: Joi.number().min(1000000000).max(9999999999).required().messages({
    "number.min": "Invalid phone number",
    "any.required":"Phone number is required",
    "number.base":"Phone number is empty",
    "number.max":"Invalid phone number"
  }),
  email: Joi.string()
  .email({ minDomainSegments: 2 })
  .required().messages({
   "any.required":"email is required",
   "string.empty":"email is empty",
   "string.email":"Email is not valid"
 }),
  password: Joi.string().min(1).required().messages({
    "string.base": "password be string",
    "string.pattern.base":"password must not have numbers",
    "any.required":"password is required",
    "string.empty":"password is empty"
  })
})


