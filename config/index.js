require('dotenv').config();
module.exports={
    port: 3001,
    databaseHOST: process.env.DB_HOST,
    databaseDBNAME:process.env.DB_NAME,
    twilio:{
      account_sid:process.env.TWILIO_ACCOUNT_SID,
      auth_token:process.env.TWILIO_AUTH_TOKEN,
      twilio_phone_number:"+13343731984"
    }
  }