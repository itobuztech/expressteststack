module.exports = class HttpError extends Error {
  constructor (status, message, errordata) {
    super(message)
    this.status = status
    this.name = 'HttpError'
    this.errordata = errordata
  }
}
