const ValidationError = require('./validation.error')
const HttpError = require('./http.error')

module.exports = {
  ValidationError,
  HttpError
}
