module.exports = class ValidationError extends Error {
  constructor (status, message, errordata) {
    super(message)
    this.status = status
    this.name = 'ValidationError'
    this.errordata = errordata
  }
}
