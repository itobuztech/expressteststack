const DefaultError = (req, res, next) => {
  var status = 404
  res.status(status).json({
    status: status,
    message: 'page not found',
    error_details: {}
  })
}
const CustomError = (err, req, res, next) => {
  var status = err.status
  res.status(status).json({
    status: status,
    message: err.message,
    body: {
      error_details: {
        type: err.name,
        data: err.errordata
      }
    }
  })
}
module.exports = {
  CustomError,
  DefaultError
}
