const Usermodel = require('../models/user')
const config=require('../config/index')
var twilio = require('twilio');
const Signup=async(user,hashing)=>{
//const accountSid=config.twilio.account_sid
//const authToken = config.twilio.auth_token;
/*var client = new twilio(accountSid, authToken);
console.log(accountSid)
console.log(authToken)
console.log(config.twilio.twilio_phone_number)
client.messages.create({
    to: "+918910412709",
    from: "+13343731984",
     body: 'Your One Time Password For Itnews'
   })
  //.then(message => console.log(message.sid))
 // .catch(err=>console.log(err))*/
  const usersignupdate = new Usermodel({
    name: user.name,
    address: user.address,
    country_code:user.country_code,
    phone_number:user.phone_number,
    email: user.email,
    password: hashing
  })
  const storedata = await usersignupdate.save();
  return storedata
}
const UserAllDetail=async()=>{
  const getUserData = await Usermodel.find();
  return getUserData
}
const SingleUserDetail=async(userid)=>{
  const getsingleuser= await Usermodel.findById(userid)
  return getsingleuser
}
const DeleteUser=async(userid)=>{
  const getsingleuser= await Usermodel.findByIdAndRemove(userid)
  return getsingleuser
}

const UpdateUser=async(userid,userdata)=>{
 const updateddata = await Usermodel.findByIdAndUpdate(userid, { $set: userdata })
 return updateddata
}

module.exports={
  Signup,
  UserAllDetail,
  SingleUserDetail,
  DeleteUser,
  UpdateUser
}