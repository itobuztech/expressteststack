const Postmodel = require('../models/post')
const Usermodel = require('../models/user')
const config=require('../config/index')
const twilio = require('twilio');
const accountSid=config.twilio.account_sid
const authToken = config.twilio.auth_token;
var client = new twilio(accountSid, authToken);
const mongoose = require('mongoose')
const PostSchema=require('../controllers/schemas/PostSchema')
// get post by post id
const getPost=async(postid)=>{
	const singlePost = await Postmodel.findById({_id: postid })
	return singlePost
}
// create new post
const addingPost=async(postObj,userid)=>{
	const PostmodelData = new Postmodel({
		_id: new mongoose.Types.ObjectId,
		title: postObj.title,
		subtitle:postObj.subtitle,
		message: postObj.message,
		users: userid
	})
	const addnewpost = await PostmodelData.save()
	if(addnewpost){
		const getOthersUserid=await Usermodel.find({ "_id": { "$ne": userid } })
		.select('phone_number country_code')
		const UserPhoneArray=[]
		if(getOthersUserid.length ==0 || getOthersUserid==null){
			var result={
				status:true,
				message: 'Your post has been created successfully',
				data: addnewpost,
				statusCode: 201
			}
		}else{
			getOthersUserid.forEach((phoneResult)=>{
				if("country_code" in phoneResult || typeof phoneResult.country_code !== undefined || typeof phoneResult.phone_number !== undefined){
					UserPhoneArray.push({country_code:phoneResult.country_code,phone_number:phoneResult.phone_number})
				}
			})
			UserPhoneArray.forEach((presult)=>{
				client.messages.create({
					to: '+'+presult.country_code+presult.phone_number,
					from: "+13343731984",
					body: 'New post added in itnews.Please login to see that post'
				})
				.then((message)=>console.log(message))
			})
			var result={
				status:true,
				message: 'Your post has been created successfully',
				data: addnewpost,
				statusCode: 201
			}
		}
	}else{
		var result={
			status:false,
			message: 'Your post has not been created successfully',
			data: addnewpost,
			statusCode: 501
		}
	}
	return result
}

// all post list 
const GetAllPost=async(userid)=>{
	const allpost = await Postmodel.find({ users: userid }).populate()
	if(allpost){
		var result={
			status:true,
			message: 'Your post has been listed successfully',
			resultData: allpost,
			statusCode: 200
		}
	}else{
		var result={
			status:false,
			message: 'Your post has not been listed successfully',
			resultData: allpost,
			statusCode: 501
		}
	}
	return result
}
// delete post baseed on postid
const deletePostService=async(postid)=>{
	const singlePost = await Postmodel.findById(postid)
	if(singlePost){
		const deletepostData=await Postmodel.findByIdAndDelete(postid)
		if(deletepostData){
			var postresult={
				status:true,
				message:"Post has been deleted successfully",
				statusCode:200
			}
		}else{
			var postresult={
				status:false,
				message:"Post has not been deleted successfully",
				statusCode:501
			}
		}	
	}else{
		var postresult={
			status:false,
			message:"Post was not found",
			statusCode:404
		}
	}
	return postresult
}
// modify update service
const updateService=async(postId,userid,postObj)=>{
	const singlePost = await Postmodel.findById(postId)
	if(singlePost){
		const PostModifiedData = {
			title: postObj.title,
			subtitle: postObj.subtitle,
			message: postObj.message
		  }
		  const {error,value} = PostSchema.validate(PostModifiedData)
		  if (error) {
			var postresult={
				status:false,
				message:"validation error",
				statusCode:400
			}
		  } else{
			  
			const filter = { _id: postId }
			const update = { title: value.title, subtitle: value.subtitle, message: value.message }
			const modiData = await Postmodel.findOneAndUpdate(filter, update)
			if (modiData) {
				var postresult={
					status:true,
					message:"Your post has been successfully Updated",
					statusCode:200
				}
		   } else {
			var postresult={
				status:false,
				message:"Your post has not been successfully Updated",
				statusCode:304
			}
		  }
		  }
	}else{
		var postresult={
			status:false,
			message:"Your was not found",
			statusCode:404
		}
	}
	return postresult
}
module.exports={
	GetAllPost,
	getPost,
	addingPost,
	deletePostService,
	updateService
}