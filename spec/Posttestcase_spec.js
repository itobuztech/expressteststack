const frisby = require('frisby')
var user_baseurl='http://localhost:3001/api/asynuseroperation'
var post_url='http://localhost:3001/api/PostOperation'
frisby.create('Test for user login api')
.post(user_baseurl+'/login', {
	email: 'sanjukta@gmail.com',
	password: '123456'
},	
{json:true})
.expectStatus(200)
.afterJSON(function (json) {
	frisby.globalSetup({
		request: {
			headers: {
				'Authorization': 'Bearer ' + json.token
			}
		}
	})
	frisby.create('Test for create post  api')
	.post(post_url+'/addpost',{
		title:"Headlines",
		subtitle:"health",
		message:"testing"
	},
	{ json: true })
	.expectStatus(200)
	.expectHeaderContains('content-type', 'application/json')
	.toss();
	frisby.create('Test for post listing api')
	.get(post_url+'/allposts')
	.expectStatus(200)
	.toss()
	frisby.create('Test for update post api')
	.put(post_url+'/updatepost/5e904130b10d73234cf628a9',{
		title:"Current Headlines",
		subtitle:"health",
		message:"testing10"
	},{json: true})
	.expectStatus(200)
	.toss()
	frisby.create('Test for delete post api')
	.delete(post_url+'/deletepost/5e904130b10d73234cf628a9')	
	.expectStatus(200)
	.toss()
})
.toss()



