const frisby = require('frisby')
var user_baseurl='http://localhost:3001/api/asynuseroperation'
frisby.create('Test for user registration api')
.post(user_baseurl+'/add-user',{
	name:"sanjukta10 das",
	address:"kolkata",
	phone_number:"8541203698",
	email:"sanjukta@gmail.com",
	password:"123456789"
},
{ json: true })
.expectStatus(200)
.expectHeaderContains('content-type', 'application/json')
.toss();
frisby.create('Test for user login api')
.post(user_baseurl+'/login', {
	email: 'sanjukta@gmail.com',
	password: '123456'
},	
{json:true})
.expectStatus(200)
.afterJSON(function (json) {
	frisby.globalSetup({
		request: {
			headers: {
				'Authorization': 'Bearer ' + json.token
			}
		}
	})
	frisby.create('Test for user listing api')
	.get(user_baseurl+'/list-user')
	.expectStatus(200)
	.toss()
	frisby.create('Test for single user api')
	.get(user_baseurl+'/user-details/5e902e3c20b89a1dc08fd6e0')
	.expectStatus(200)
	.toss()
	frisby.create('Test for user update api')
	.put(user_baseurl+'/update-user/5e902e3c20b89a1dc08fd6e0',{
		name:"Rima das",
		address:"Delhi",
		phone_number:"1234567890",
		email:"rima@gmail.com",
		password:"123456"
	},{json: true})
	.expectStatus(200)
	.toss()
	frisby.create('Test for user delete api')
	.delete(user_baseurl+'/delete-user/5e902e3c20b89a1dc08fd6e0')
	.expectStatus(200)
	.toss()
})
.toss()



