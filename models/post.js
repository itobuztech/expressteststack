const mongoose = require('mongoose')
const postSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId
  },
  title: {
    type: String
  },
  subtitle: {
    type: String
  },
  message: {
    type: String
  },
  users: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user'
  }

})
module.exports = mongoose.model('post', postSchema)
