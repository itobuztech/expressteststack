const mongoose = require('mongoose')
const validator = require('validator')
const userschema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    minlength: 3,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  country_code:{
    type:Number,
    required:true
  },
  phone_number: {
    type: Number,
    required: true
  },
  email: {
    type: String,
    trim: true,
    required: true
  },
  password: {
    type: String,
    required: true
  }
  // post: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'post',
  //   required: true
  // }
})
module.exports = mongoose.model('user', userschema)
